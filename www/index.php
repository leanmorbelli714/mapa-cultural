<?php 
    include 'conexion.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mapa Cultural</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<h1 class="title">Busqueda de eventos por zonas</h1>



<div class="custom-input-file col-md-6 col-sm-6 col-xs-6">
<form method="POST">
<label for="locality">FILTRAR POR LOCALIDADES</label>
<input list="locality" name="search">
<datalist id="locality">
<?php $QUERY = 'SELECT * FROM Localities';?>
    <?php $result = mysqli_query($link, $QUERY);?>
    <?php
    if($result){
        while($row = mysqli_fetch_assoc($result)){
            echo '<option value="'.$row['locality'].'">
            '; 
        }
    }
    ?>
<
</datalist>
<input type="submit" value="Buscar">
<a href="index.php">Clear</a>
</form>
</div>

<br>


<div class="custom-input-file col-md-6 col-sm-6 col-xs-6">
<form method="POST">
<label for="category">FILTRAR POR CATEGORIAS</label>
<input list="category" name="search2">
<datalist id="category">
<?php $QUERY = 'SELECT * FROM Categories';?>
    <?php $result = mysqli_query($link, $QUERY);?>
    <?php
    if($result){
        while($row = mysqli_fetch_assoc($result)){
            echo '<option value="'.$row['category'].'">
            '; 
        }
    }
    ?>

</datalist>
<input type="submit" value="Buscar">
<a href="index.php">Clear</a>
</form>
</div>




<?php

$busqueda = htmlspecialchars($_POST['search2']);

$QUERY = 'SELECT * FROM Activities_Normalized_View' ;?>
<?php $result = mysqli_query($link, $QUERY);?>
<?php
if($result){
    while($row = mysqli_fetch_assoc($result)){
        if($row["category"] == $busqueda){
        echo '  <table>
                <tr>
                <td>'.$row["category"].'<td/>
                <td>'.$row["name"].'<td/>
              <tr/>
              </table>
        '; 
        }
    }
    
}


?>

<?php




$busqueda = htmlspecialchars($_POST['search']);


$QUERY = 'SELECT * FROM Activities_Normalized_View' ;?>
<?php $result = mysqli_query($link, $QUERY);?>
<table align="center">
<tr>
                <th>Categoria</th>
                <th>Nombre</th>
                <th>Direccion</th>
                <th>Telefono</th>
                <th>Mail</th>
                <th>Web</th>
                
</tr>



<?php
if($result){
    while($row = mysqli_fetch_assoc($result)){
        if($row["locality"] == $busqueda){
        echo ' 
                
                <tr>
                <td>'.$row["category"].'<td/>
                <td>'.$row["management"].'<td/>
                <td>'.$row["adress"].'<td/>
                <td>'.$row["phone"].'<td/>
                <td>'.$row["email"].'<td/>
                <td>'.$row["web"].'<td/>
              </tr>
              
        '; 
        }
    }
    
}

?>

</table>

<?php /*$QUERY = 'SELECT * FROM actividades_vista';?>
    <?php $result = mysqli_query($link, $QUERY);?>
    <?php
    if($result){
        while($row = mysqli_fetch_assoc($result)){
            echo '<tr>
                    <td>'.$row["localidad"].'<td/>
                    <td>'.$row["categoria"].'<td/>
                  <tr/>
            '; 
        }
    }*/
    ?>

   
</body>
</html>